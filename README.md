# DG demo

This is a small demo for a d3-powered dashboard

## Running it

yarn|npm serve

## Under the hood

Generated with [vue-cli](https://github.com/vuejs/vue-cli/), it uses [d3](https://d3js.org/) to render the charts.
