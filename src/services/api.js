const protocols = [
  'IPv6', 'IPv4', 'CoAP/CoRE', '6LoWPAN/RPL', 'KNX',
  'BACnet', 'ZigBee', 'Z-Wave', 'EnOcean', 'Modbus', 'NFC', 'RFID', 'DALI', 'M-Bus',
  'oBIX', 'X10', 'GSM/GPRS', 'WiFi', 'Bluetooth', 'UPnP', 'DLNA', 'EnergyWise', 'SenML',
  'ISA100.11a', 'WirelessHART'
]

export function getTeamProgress () {
  return new Promise((resolve, reject) => resolve([
    {username: 'Julia', protocol: protocols[3], status: 0},
    {username: 'Fred', protocol: protocols[5], status: 2},
    {username: 'Steve', protocol: protocols[5], status: 1},
    {username: 'Amelia', protocol: protocols[7], status: 2},
    {username: 'Theo', protocol: protocols[9], status: 0},
    {username: 'Suzy', protocol: protocols[11], status: 1}
  ]))
}

export function getProtocols () {
  return new Promise((resolve, reject) => resolve(protocols.filter(p => (Math.random() > 0.7)).map(id => {
    let tests = Math.floor(Math.random() * 100)
    let successes = Math.floor(Math.random() * (tests * 0.9))
    let inProgress = Math.floor(Math.random() * (tests * 0.1))
    let failures = tests - successes - inProgress
    return {
      id,
      successes,
      inProgress,
      failures,
      tests
    }
  })))
}
