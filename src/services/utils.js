const isCssSmoothSCrollSupported = () => 'scrollBehavior' in document.documentElement.style

export function smoothScrollTo (target) {
  let distance = target.getBoundingClientRect().top

  if (isCssSmoothSCrollSupported()) {
    console.log('aha!')
    return window.scrollTo({behavior: 'smooth', top: distance})
  }

  let start = window.pageYOffset

  let duration = 500
  let timeStart
  let timeElapsed

  requestAnimationFrame(time => {
    timeStart = time
    loop(time)
  })

  function loop (time) {
    timeElapsed = time - timeStart
    window.scrollTo(0, easeInOutQuad(timeElapsed, start, distance, duration))
    if (timeElapsed < duration) {
      requestAnimationFrame(loop)
    } else {
      window.scrollTo(0, start + distance)
    }
  }
}

// Robert Penner's easeInOutQuad - http://robertpenner.com/easing/
function easeInOutQuad (t, b, c, d) {
  t /= d / 2
  if (t < 1) return c / 2 * t * t + b
  t--
  return -c / 2 * (t * (t - 2) - 1) + b
}
